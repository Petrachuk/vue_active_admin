{{- define "resources_app" }}
resources:
  requests:
    memory: {{ first (pluck .Values.global.env .Values.resources.app.requests.memory) | default .Values.resources.app.requests.memory._default | quote }}
    cpu: {{ first (pluck .Values.global.env .Values.resources.app.requests.cpu) | default .Values.resources.app.requests.cpu._default | quote }}
  limits:
    memory: {{ first (pluck .Values.global.env .Values.resources.app.limits.memory) | default .Values.resources.app.limits.memory._default | quote }}
{{- end }}


