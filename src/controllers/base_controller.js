import QueryHelper from '../helpers/query_helper';

export default class BaseController {

    static actions(entity) {
        return {
            async index(params) {
                let data = {}, 
                    error = null;

                data.ready = false;

                await QueryHelper.get_data(entity.urls.index, params).then(data_response => {

                    if (data_response.respond) {
                        data.total_items = data_response.total_items;
                        data.objects = data_response.data;
                        data.link_expires = data_response.link_expires;

                        data.ready = true;
                    } else {
                        error = data_response.error;
                    }
                })
                
                return {
                    data: data,
                    error: error
                }
            },
            async show(params) {
                let data = {}, 
                    error = null;
                
                data.ready = false;

                await QueryHelper.get_data(entity.urls.show, params).then(data_response => {
                    if (data_response.respond) {
                        data.item = data_response.data;
                        data.ready = true;
                    } else {
                        error = data_response.error
                    }
                })

                return {
                    data: data,
                    error: error
                }
            }
        }
    }

    
}