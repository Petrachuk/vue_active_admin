import BaseController from './base_controller';
import Entity from '../models/tid';

export default class TidsController {
    static actions = {
        index: function(params) {
            return BaseController.actions(Entity).index(params)
        },
        show: function(params) {
            return BaseController.actions(Entity).show(params)
        }
    }
}