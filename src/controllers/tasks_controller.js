import BaseController from './base_controller';
import Entity from '../models/task';
import QueryHelper from '../helpers/query_helper';

export default class TasksController {
    static actions = {
        index: function(params) {
            return BaseController.actions(Entity).index(params)
        },
        show: function(params) {
            return BaseController.actions(Entity).show(params)
        },
        reset_task: async function(params) {
            let data = {}, 
                error = null;

            data.ready = false;

            await QueryHelper.get_data(Entity.urls.reset_task, params).then(data_response => {

                if (data_response.respond) {
                    data.ready = true;
                } else {
                    error = data_response.error;
                }
            })
            
            return {
                data: data,
                error: error
            }
        },
        get_download_url: async function(params) {
            let data = {}, 
                error = null;

            data.ready = false;

            await QueryHelper.get_data(Entity.urls.get_download_url, params).then(data_response => {
                if (data_response.respond) {
                    data.url = data_response.data;
                    data.ready = true;
                } else {
                    error = data_response.error
                }
            })

            
            return {
                data: data,
                error: error
            }
        },
        create: async function(params) {
            let data = {}, 
                error = null;

            data.ready = false;

            await QueryHelper.get_data(Entity.urls.create, params).then(data_response => {

                if (data_response.respond) {
                    data.ready = true;
                } else {
                    error = data_response.error;
                }
            })
            
            return {
                data: data,
                error: error
            }
        }
    }
}