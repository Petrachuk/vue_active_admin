import BaseController from './base_controller';
import Entity from '../models/card';

export default class CardsController {
    static actions = {
        index: function(params) {
            return BaseController.actions(Entity).index(params)
        },
        show: function(params) {
            return BaseController.actions(Entity).show(params)
        }
        
    }
}