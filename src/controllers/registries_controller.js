import BaseController from './base_controller';
import Entity from '../models/registry';

export default class RegistriesController {
    static actions = {
        index: function(params) {
            return BaseController.actions(Entity).index(params)
        },
        show: function(params) {
            return BaseController.actions(Entity).show(params)
        }
    }
}