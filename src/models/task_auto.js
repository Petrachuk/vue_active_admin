import AppSettings from "../settings/app_settings";
import Base from './base';
import CarrierCollection from './collections/carrier';
import Controller from '../controllers/tasks_auto_controller'

export default class Task {

    static Controller = Controller;

    static signatures = {
        items() {return 'Список задач'} ,
        item(item) {return `Задача ${item}`},
        add_item(){return 'Добавить задачу'} ,
        delete_item() {return ''},
        new() {return 'Создание задачи'}
    }

    static table_properties() {
        let options = {
            sortable: true,
            blocked: true
        }

        return {
            options,
            items: [
                Base.table_prop('ID', 'Id'),
                Base.table_prop('Дата', 'date', {view: 'ColumnDate'}),
                Base.table_prop('Начата', 'started_at', {view: 'ColumnDate', format: 'time'}),
                Base.table_prop('Закончена', 'finished_at', {view: 'ColumnDate', format: 'time'}),
                Base.table_prop('Перевозчик', 'carrier_code', {collection: CarrierCollection}),
                Base.table_prop('Номер', 'order_num'),
                Base.table_prop('Статус', 'status'),
                Base.table_prop('Stage', 'stage'),
                Base.table_prop('Token', 'token'),
                Base.table_prop('user id', 'user_id'),
                Base.table_prop('Final File', 'Получить ссылку', {
                    view: 'ColumnButton',
                    action: 'get_download_url',
                    object: true,
                    value: 'final_file',
                    live: true,
                    query_keys: ["s3Key", "bucket", "expires"],
                    query_values: ['value.s3Key', 'value.bucket', 'this.expires']
                }),
                Base.table_prop('Abs Fule', 'Получить ссылку', {
                    view: 'ColumnButton',
                    action: 'get_download_url',
                    object: true,
                    value: 'abs_file',
                    live: true,
                    query_keys: ["s3Key", "bucket", "expires"],
                    query_values: ['value.s3Key', 'value.bucket', 'this.expires']
                }),

            ] 
        }
    }
    
    static form_fields() {
        let options = {}

        return {
            options,
            items: [
               
            ] 
        }
    } 
        
    static table_actions = [
        // {
        //     name: 'new',
        //     text: 'Добавить задачу',
        //     path: '/tasks/new'
        // }
    ]
           
    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/ResolveGetTaskList`,
            method: 'post',
            data_place: 'list'
        },
        get_download_url: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/StorageGetDownloadUrl`,
            method: 'post',
            data_place: ''
        }
    } 

    static default_params = {
        
    }

    static routes = {
        index: `/tasks`,
        new: `/tasks/new` 
    }

}