import AppSettings from "../settings/app_settings";
import Base from './base';
import Controller from '../controllers/tids_controller'
import CarrierCollection from './collections/carrier';
import GroupCollection from './collections/group';

export default class Tid {

    static Controller = Controller;

    static signatures = {
        items() {return 'Тиды'} ,
        item(item) {return `Тид ${item}`},
        add_item(){return ''} ,
        delete_item() {return ''}
    }

    static table_properties() {
        let options = {
            sortable: false,
            blocked: true
        }

        return {
            options,
            items: [
                Base.table_prop('ID', 'ID'),
                Base.table_prop('TID', 'TID'),
                Base.table_prop('Заблокирован', 'IsLocked', {view: 'ColumnBool', yes: 'Заблокирован', no: '-'}),
                Base.table_prop('Заблокирован в', 'LockedAt', {view: 'ColumnDate', format: 'time'}),
                Base.table_prop('Группа', 'Group', {collection: GroupCollection}),
                Base.table_prop('Перевозчик', 'Carrier', {collection: CarrierCollection}),
            ] 
        }
    }
    
    static form_fields() {
        let options = {}

        return {
            options,
            items: [
            ] 
        }
    } 
        
           
    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGatewayPublic/GetTIDs`,
            method: 'post',
            data_place: 'TIDs'
        },
        show: {
            // url: `${AppSettings.api_url}/twirp/proto.ApmAPIGateway/PassGetPass`,
            // method: 'post',
            // data_place: ''
        }
    } 

    static default_params = {
        
    }

    static routes = {
        index: `/tids`,
        show: `//` 
    }

}