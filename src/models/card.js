import AppSettings from "../settings/app_settings";
import Base from './base';

export default class Card {
    
    static signatures = {
        items() { return 'Карты' },
        item(item) { return `Карта ${item}` },
        add_item() { return 'Добавить карту' },
        delete_item() { return 'Удалить карту' },
    }

    static table_properties() {
        let options = {
            sortable: true,
    
        }

        return {
            options,
            items: [
                Base.table_prop('ID', 'id'),
                Base.table_prop('ID платежной сисемы', 'system'),
                Base.table_prop('Тип карты', 'type'),
                Base.table_prop('BIN', 'bin'),
                Base.table_prop('PAN', 'pan'),
                Base.table_prop('Срок действия (MMYY)', 'exp')
            ]
        }
    } 

    static form_fields() {
        let options = {}
        
        return {
            options,
            items: [
                Base.form_field('ID', 'id'),
                Base.form_field('Идентификатор платёжной системы', 'system'),
                Base.form_field('Тип карты', 'type'),
                Base.form_field('Хэшированный PAN карты', 'pan'),
                Base.form_field('BIN карты', 'bin'),
                Base.form_field('Срок действия карты (MMYY)', 'exp'),
                Base.form_field('Зашифрованный блок данных по карте (EMV)', 'emv', {component: 'TextArea'}),
                Base.form_field('Токен', 'token')
            ]
        }
    } 

    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/PassGetCards`,
            method: 'post', 
            data_place: 'cards'
        },
        show: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/PassGetCard`,
            method: 'post',
            data_place: ''
        }
    } 

    static default_params = {
        
    }

    static routes = {
        index: `/cards`,
        show: `/cards/` 
    }

}