import AppSettings from "../settings/app_settings";
import Base from './base';
import Controller from '../controllers/registries_controller'

export default class Registry {

    static Controller = Controller;

    static signatures = {
        items() {return 'Реестры'},
        item(item){return `Реестр ${item}`},
        add_item() {return 'Добавить'} ,
        delete_item() {return 'Удалить'} 
    }

    static table_properties() {
        let options = {
            sortable: true,
    
        }

        return {
            options,
            items: [
                Base.table_prop('ID', 'id'),
                Base.table_prop('Статус', 'status'),
                Base.table_prop('Файл', 'file'),
                Base.table_prop('Сумма', 'sum')
            ] 
        }
    } 

    static form_fields() {
        let options = {}
        
        return {
            options,
            items: [
                Base.form_field('ID', 'id'),
                Base.form_field('Создан', 'created'),
                Base.form_field('Sent', 'sent'),
                Base.form_field('Response Received', 'responseReceived'),
                Base.form_field('File', 'file'),
                Base.form_field('Status', 'status'),
                Base.form_field('Sum', 'sum'),
                Base.form_field('Billing Period', 'billingPeriod'),
            ] 
        }
    } 
           

    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/UserGetServiceSchema`,
            method: 'post',
            data_place: 'registries'
        },
        show: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/AbsGetRegistry`,
            method: 'post',
            data_place: ''
        }
    } 

    static default_params = {
        
    }

    static routes = {
        index: `/registries`,
        show: `/registries/` 
    }

}