import AppSettings from "../settings/app_settings";
import Base from './base';
import Controller from '../controllers/authorizations_controller';
import Collections from "../settings/collections";
import CarrierCollection from './collections/carrier';
import CardSystemCollection from './collections/card_system';
import ErrorCodeCollection from './collections/error_code';


export default class Authorization {

    static Controller = Controller;

    static signatures = {
        items() {return 'Авторизации'},
        item(item) {return `Авторизация ${item}`},
        add_item() {return 'Добавить'},
        delete_item() {return 'Удалить'}
    }

    static table_properties() {
        let options = {
            sortable: false,
            blocked: true,
            filters: [
                {
                    field: 'pan',
                    label: 'PAN',
                    type: 'FilterInput',
                    settings: [
                        {
                            operator_id: Collections.filter_operator['equal'],
                            prefix: ''
                        },
                    ]
                },
                
            ]
        }

        return {
            options,
            items: [
                Base.table_prop('ID', 'id'),
                // Base.table_prop('PAN', 'card_pan'),
                Base.table_prop('Сумма', 'amount', {calculated: '/100'}),
                Base.table_prop('Статус', 'auth_status'),
                Base.table_prop('ПС', 'card_pay_system', {collection: CardSystemCollection}),
                Base.table_prop('Мобильный кошелек', 'card_wallet_id'),
                Base.table_prop('Перевозчик', 'carrier_code', {collection: CarrierCollection}),
                Base.table_prop('Создано', 'created_at', {view: 'ColumnDate', format: 'time'}),
                Base.table_prop('Ответ банка', 'error_code', {collection: ErrorCodeCollection, dynamic_collection: true}),
                Base.table_prop('Отменено', 'is_canceled', {view: 'ColumnBool', yes: 'Отменено', no: '-'}),
                Base.table_prop('RRN', 'rrn'),
                Base.table_prop('Турникет', 'terminal_id'),
            ]
        }
    } 

    static show_fields() {
        
    }

    static form_fields() {
        let options = {}

        return {
            options,
            items: [
                
            ] 
        }
    } 
           

    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/GetAuthorizeList`,
            method: 'post',
            data_place: 'List'
        },
        show: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/PassGetAuthorization`,
            method: 'post',
            data_place: ''
        }
            
    } 

    static default_params = {}

    static routes = {
        index: `/authorizations`,
        show: `/authorizations/` 
    }

}