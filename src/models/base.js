export default class Base {

    static table_prop(text, val, options = {}) {
        let default_options = {
            visible: true,
            view: "ColumnText",
            yes: 'Да',
            no: 'Нет'
        }

        Object.assign(default_options, options)

        return { 
            caption: {
                text: text, 
                class: `table_${val}`
            },
            value: val,
            block: {
                view: default_options.view,
                visible: default_options.visible,
                options: default_options
            }
        }
    }

    static show_field(text, val, options={}) {
        let default_options = {
            component: 'ShowText',
        }

        Object.assign(default_options, options)

        return {
            text: text,
            block: {
                component: default_options.component,
                content: {
                    value: val,
                    options: default_options
                }
            }
        }
    }

    static form_field(text, val, options={}) {
        let default_options = {
            component: 'Input',
            readonly: true,
            rows: 6,
            format: ''
        }

        Object.assign(default_options, options)

        return {
            caption: {
                text: text,
                class: `model_${val}`
            },
            block: {
                component: default_options.component,
                content: {
                    value: val,
                    options: default_options,
                    default: '' 
                }
            },
            validates: [
                {
                    rule: null, // правило валидации
                    message: null
                },
            ]
        }
    }
}