

export default class Carrier {

    static values = {
        0: 'Результат не определен',
        1: 'Неизвестно',
        2: 'VISA',
        3: 'MasterCard',
        4: 'MIR',
        5: 'CUP'
    }

    static collection() {
        let arr = []

        var _this = this;

        Object.keys(this.values).forEach(function(item, i) {
            arr.push({
                name: _this.values[item],
                value: i
            })
        })

        return arr;
    }
}