

export default class Carrier {

    static values = {
        0: 'Перевозчик не определен',
        1: 'Московское метро',
        2: 'Мосгортранс',
        3: 'МЦД',
        4: 'МТППК',
        5: 'Аэроэкспресс'
    }

    static collection() {
        let arr = []

        var _this = this;

        Object.keys(this.values).forEach(function(item, i) {
            arr.push({
                name: _this.values[item],
                value: i
            })
        })

        return arr;
    }
}