

export default class Carrier {

    static values(val) {
        let result = ''

        if (val < 11) {
            result = 'Успешно'
        } else if (val > 11 && val != 76) {
            result = 'Отказ'
        } else if (val == 76) {
            result = 'Отказ по счету'
        }

        return result;
    }

    
}