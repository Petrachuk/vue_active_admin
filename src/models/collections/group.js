

export default class Group {

    static values = {
        0: 'GROUP_UNKNOWN',
        1: 'GROUP_ONLINE_AUTH',
        2: 'GROUP_OFFLINE_AUTH',
        3: 'GROUP_REAUTH',
    }

    static collection() {
        let arr = []

        var _this = this;

        Object.keys(this.values).forEach(function(item, i) {
            arr.push({
                name: _this.values[item],
                value: i
            })
        })

        return arr;
    }
}