

export default class TapPassKind {

    static values = {
        1: 'TAP',
        2: 'PASS_ONLINE',
        3: 'PASS_OFFLINE',
        4: 'PASS_FAILED'
    }
}