import AppSettings from "../settings/app_settings";
import Base from './base';
import tap_pass_kind from './collections/tap_pass_kind';
import Controller from '../controllers/passes_controller';

export default class Tap {

    static Controller = Controller;

    static signatures = {
        items() {return 'Проходы'} ,
        item(item) {return `Проход ${item}`},
        add_item(){return ''} ,
        delete_item() {return ''}
    }

    static table_properties() {
        let options = {
            sortable: true,
        }


        return {
            options,
            items: [
                Base.table_prop('ID', 'id'),
                Base.table_prop('Тип', 'kind', {collection: tap_pass_kind}),
                Base.table_prop('Sign', 'sign'),
                Base.table_prop('Создано', 'created_at_carrier', {view: 'ColumnDate'}),
                Base.table_prop('Авторизован', 'is_auth'),
            ] 
        }
    }

    static show_fields() {
        let options = {}

        return {
            options,
            items: [
                Base.show_field('ID', 'id'),
                Base.show_field('Card ID', 'card_id'),
                Base.show_field('Тип', 'kind'),
                Base.show_field('Carrier tap ID', 'carrier_tap_id'),
                Base.show_field('Перевозчики', 'carrier_code'),
                Base.show_field('Авторизован', 'is_auth'),
                Base.show_field('Carrier resolution', 'carrier_resolution'),
                Base.show_field('Terminal ID', 'terminal_id'),
                Base.show_field('Terminal Station', 'terminal_station'),
                Base.show_field('Terminal Direction', 'terminal_direction'),
                Base.show_field('Is cancel', 'is_cancel'),
                Base.show_field('Is complex timeout', 'is_complex_timeout'),
                Base.show_field('Created at request', 'created_at_request', {format: 'date'}),
                Base.show_field('Created at carrier', 'created_at_carrier', {format: 'date'}),
            ] 
        }
    } 
    
    static form_fields() {
        let options = {}

        return {
            options,
            items: [
                
            ] 
        }
    } 
        
           
    static urls = {
        index: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/PassGetPasses`,
            method: 'post',
            data_place: 'passes'
        },
        show: {
            url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/PassGetPass`,
            method: 'post',
            data_place: ''
        }
    } 

    static default_params = {
        
    }

    static routes = {
        index: `/passes`,
        show: `/passes/` 
    }

}