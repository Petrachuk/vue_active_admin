
var AppSettings = {
  api_url() {
    let url = null;

    switch(window.location.origin) {
      case 'https://apm-frontend.transport.multicarta.ru':
        url = 'https://apm-gateway.transport.multicarta.ru';
        break;
      case 'https://apm-frontend-stage.transport.siroccotechnology.ru':
        url = 'https://apm-gateway-stage.transport.siroccotechnology.ru';
        break;
      default:
        url = 'https://apm-gateway-test.transport.siroccotechnology.ru';

    }

    return url;
  } 
};

export default AppSettings;
