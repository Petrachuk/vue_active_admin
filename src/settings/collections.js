
const Collections = {
    filter_operator: {
        greater: 1,
        greater_or_equal: 2,
        equal: 3,
        less: 4,
        less_or_equal: 5,
        is_not: 6
    },
    sort_order: {
        asc: 1,
        desc: 2
    }
};
  
  export default Collections;
  