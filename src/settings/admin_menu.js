
var AdminMenu = [
    {
        path: '/taps',
        icon: 'content_paste',
        text: 'Тапы',
        component: 'Link'
    },
    {
        path: '/passes',
        icon: 'content_paste',
        text: 'Проходы',
        component: 'Link'
    },
    // {
    //     path: '/cards',
    //     icon: 'content_paste',
    //     text: 'Карты'
    // },
    {
        path: '/registries',
        icon: 'content_paste',
        text: 'Реестры',
        component: 'Link'
    },
    {
        path: '/tids',
        icon: '',
        text: 'Тиды',
        component: 'Link'
    },
    {
        path: '/authorizations',
        icon: '',
        text: 'Авторизации',
        component: 'Link'
    },
    {
        path: '#',
        icon: '',
        text: 'Задачи',
        component: 'Dropdown',
        items: [
            {
                path: '/tasks',
                icon: '',
                text: 'Задачи на сверку'
            },
            {
                path: '/tasks_auto',
                icon: '',
                text: 'Задачи на автосверку'
            }
        ]
    }
    
]
    
  
  
  export default AdminMenu;
  