import axios from 'axios';
import AppSettings from '../settings/app_settings';
import Vue from 'vue'
import router from '../router/router';

export default class QueryHelper {
    
    static headers() {
        return {
            Authorization: `Bearer ${localStorage.access_token}`,   
            // Origin: 'http://localhost:8080'
        }
    }

    static async refresh_token() {
        return await axios
            ({
                method: 'post',
                url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGatewayPublic/RefreshToken`,
                data: {
                    token: localStorage.refresh_token
                }
            })
            .then(response => {
                localStorage.access_token = response.data.access_token
                localStorage.refresh_token = response.data.refresh_token

                router.push(router.history.current)
            })
            .catch(error => {
                console.log(error.response)
            })
    }   

    static async logout() {
        axios
            ({
                method: 'POST',
                url: `${AppSettings.api_url()}/twirp/proto.ApmAPIGateway/Logout`,
                data: {},
                headers: this.headers()
            })
            .then(response => {
                localStorage.access_token = null
                localStorage.refresh_token = null

                router.push('/auth')
            })
            .catch(error => {
                console.log(error)

                Vue.notify({
                    group: 'foo',
                    title: `Ошибка!`,
                    text: error.response.data.msg,
                    type: 'error',
                    duration: 6000,
                    width: '400px',
                })

            })
    }

    static async get_data(url_obj, params) {

        let api_response = {
            respond: false,
            error: {
                status: 200
            }
        }

        await axios
            ({
                method: url_obj.method,
                url: url_obj.url,
                data: params,
                headers: this.headers()
            })
            .then(response => {
                if (url_obj.data_place) {
                    api_response.data = eval(`response.data.${url_obj.data_place}`);
                } else {
                    api_response.data = response.data;
                }

                api_response.total_items = response.data.total;
                api_response.link_expires = response.data.link_expires;
                api_response.respond = true;
            })
            .catch(error => {
                console.log(error)
                api_response.respond = false
                api_response.error = error.response;

                Vue.notify({
                    group: 'foo',
                    title: `Ошибка!`,
                    text: error.response.data.msg,
                    type: 'error',
                    duration: 6000,
                    width: '400px',
                })

                if (error.response.status == 401 || error.response.status == 403) {
                    // console.log('Токен обновлен');
                    // this.refresh_token();
                    router.push('/auth')

                } 
                // else if (error.response.status == 403) {
                    // router.push('/auth')
                // }
            })
            
        return api_response;
    }
}