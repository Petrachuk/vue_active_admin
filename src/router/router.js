import Vue from 'vue';
import VueRouter from 'vue-router';

import DashboardLayout from "../pages/Layout/DashboardLayout";

import CardsIndex from '../views/pages/cards/Index';
import CardsShow from '../views/pages/cards/Show'; 

import RegistriesIndex from '../views/pages/registries/Index';
import RegistriesDetail from '../views/pages/registries/Show';

import AuthorizationsIndex from '../views/pages/authorizations/Index';
import AuthorizationsDetail from '../views/pages/authorizations/Show';

import TapsIndex from '../views/pages/taps/Index';
import TapsDetail from '../views/pages/taps/Show';

import PassesIndex from '../views/pages/passes/Index';
import PassesDetail from '../views/pages/passes/Show';

import TidsIndex from '../views/pages/tids/Index';
// TIDS show must be here

import TasksIndex from '../views/pages/tasks/Index';
import TasksAutoIndex from '../views/pages/tasks_auto/Index';


import LoginPage from '../views/pages/auth/Login';
import RegisterPage from '../views/pages/auth/Register';
import AuthPage from '../views/pages/auth/Auth';

const routes = [
  {
    path: "/auth",
    name: "Auth",
    component: AuthPage,
    redirect: "/auth/login",

    children: [
        {
          path: "/auth/register",
          name: "Register",
          component: RegisterPage
        },
        {
          path: "/auth/login",
          name: "Login",
          component: LoginPage
        }
    ]
  },
  
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/taps",

    children: [
      {
        path: "cards",
        name: "Cards",
        component: CardsIndex,
      },
      {
        path: "cards/:id",
        name: "Cards detail",
        component: CardsShow
      },
      {
        path: "registries",
        name: "Registries",
        component: RegistriesIndex
      },
      {
        path: "registries/:id",
        name: "Registries detail",
        component: RegistriesDetail
      },
      {
        path: "authorizations",
        name: "Authorizations",
        component: AuthorizationsIndex
      },
      {
        path: "authorizations/:id",
        name: "Authorizations detail",
        component: AuthorizationsDetail
      },

      {
        path: "taps",
        name: "Taps",
        component: TapsIndex
      },
      {
        path: "taps/:id",
        name: "Taps detail",
        component: TapsDetail
      },

      {
        path: "passes",
        name: "Passes",
        component: PassesIndex
      },
      {
        path: "passes/:id",
        name: "Passes detail",
        component: PassesDetail
      },
      {
        path: "tids",
        name: "Tids",
        component: TidsIndex
      },
      {
        path: "tasks",
        name: "Tasks",
        component: TasksIndex
      },
      {
        path: "tasks_auto",
        name: "TasksAuto",
        component: TasksAutoIndex
      },
    ]
  },

];


Vue.use(VueRouter)

const router = new VueRouter({
    routes,
    linkExactActiveClass: "nav-item active",
    saveScrollPosition: true,
    // mode: 'history'
});

export default router;