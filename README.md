
### Сборка проекта

```
  make build
```

или

```
  npm ci
  npm run build
```


Собранный проект будет размещен в папке dist

### Запуск

```
  npm run serve
```

или

```
    make run
```